"""Connection driver to the 24-well Plate Calibration Plate

Plate design
============

AIN can be any value from 0 to 13,
three AIN values are used per plate.

AIN 0-2 = board 1
AIN 3-5 = board 2
AIN 6-8 = board 3
etc.

Three additional multiplexers can be set by changing the registry.
This allows to measure 8 diodes per AIN value.

==== ==== ==== =====
MIO0 MIO1 MIO2 Diode
==== ==== ==== =====
0    0    0    0
1    0    0    1
0    1    0    2
1    1    0    3
0    0    1    4
1    0    1    5
0    1    1    6
1    1    1    7
==== ==== ==== ====

"""

from pycultivator.connection.labjackConnection import *
from pycultivator.core.pcParser import assert_range

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'
__all__ = [
    "LCPConnection", "FakeLCPConnection", "LCPConnectionException"
]


class LCPConnection(LabJackConnection):

    namespace = "connection.lcp"
    default_settings = {
        # default settings for this class
    }

    def __init__(self, settings=None, **kwargs):
        super(LCPConnection, self).__init__(settings=settings, **kwargs)

    @classmethod
    def getFake(cls):
        return FakeLCPConnection

    @staticmethod
    def mioToChannel(MIO0, MIO1, MIO2):
        """ Converts the multiplexer settings to the selected channel number of the multiplexer

        :param MIO0: State of multiplexer 0
        :type MIO0: int
        :param MIO1: State of multiplexer 1
        :type MIO1: int
        :param MIO2: State of multiplexer 2
        :type MIO2: int
        :return: channel number [0-8]
        :rtype: int
        """
        channel = 0
        # AIN0 = 0-7, AIN1 = 8-15, AIN2 = 16-23
        channel += MIO2 << 2
        channel += MIO1 << 1
        channel += MIO0 << 0
        return channel

    @staticmethod
    def channelToMIO(channel):
        """Converts a channel number (of the multiplexer) to a multiplexer settings.

        :param channel: Channel in the multiplexer
        :type channel: int
        :return: Tuple with MIO0, MIO1, MIO2
        :rtype: tuple[int, int, int]
        """
        # mio 2 = is 1 if value is above or equal to 4
        MIO2 = (channel & 4) >> 2
        # mio 1 = is 1 if left is above or equal to 2
        MIO0 = (channel & 2) >> 1
        # mio 0 = is 1 if left is above or equal to 1
        MIO1 = (channel & 1) >> 0
        # read value
        return MIO0, MIO1, MIO2

    @classmethod
    def wellToAddress(cls, well, board):
        """Well and board number to AIN address + MIO settings"""
        AIN = (well // 8) + (board * 3)
        channel = well % 8
        results = [AIN]
        results.extend(cls.channelToMIO(channel))
        return results

    def readWell(self, well, board, resolutionIndex=12, gainIndex=1):
        """Reads a Diode using the well position index and board information"""
        AIN = well // 8 + (board * 3)
        channel = well % 8
        self.getLog().debug(
            "Converted (BOARD {:2}, WELL {:2}) to: (AIN {:2}, Channel {:2})".format(
                board, well, AIN, channel
            )
        )
        return self.readDiode(AIN, channel, resolutionIndex=resolutionIndex, gainIndex=gainIndex)

    def readDiode(self, AIN, channel, resolutionIndex=12, gainIndex=1):
        """Reads a Diode using multiplexers

        :param AIN: Index of Analog INput on LabJack
        :param channel: Index of the channel to read, by setting multiplexer (0 <= channel <= 7)
        :param gain: Gain Index (0 = x1, 1 = x10, 2 = x100, 3 = x1000, 15 = autorange)
        :param resolution: Resolution Index (0 = default, 1-8 high-speed, 9-12 high-res)
        :return: The voltage measured
        """
        assert_range(channel, lwr=0, upr=7, name="channel", include=True)
        # select channel
        MIO0, MIO1, MIO2 = self.channelToMIO(channel)
        self.setDOState(16, MIO0)
        self.setDOState(17, MIO1)
        self.setDOState(18, MIO2)
        # measure
        return self.readAIN(AIN, resolutionIndex=resolutionIndex, gainIndex=gainIndex)

    def readBlank(self, resolutionIndex=12, gainIndex=1):
        """Reads the blank using AIN 13"""
        return self.readAIN(13, gainIndex=gainIndex, resolutionIndex=resolutionIndex)

    def readAIN(self, channel, resolutionIndex=12, gainIndex=1):
        """Reads the voltage """
        assert_range(channel, 0, 13, name="AIN")
        if gainIndex not in (0, 1, 2, 3, 15):
            raise ValueError("Invalid value for gain: {}".format(gainIndex))
        assert_range(resolutionIndex, 0, 12, name="resolution")
        return super(LCPConnection, self).readAIN(channel, resolutionIndex, gainIndex)


class FakeLCPConnection(LCPConnection, FakeLabJackConnection):

    def __init__(self, settings=None, **kwargs):
        super(FakeLCPConnection, self).__init__(settings=settings, **kwargs)


class LCPConnectionException(LabJackException):
    """Exception raise by the connection driver to the LC Plate"""
    
    def __init__(self, msg):
        super(LCPConnectionException, self).__init__(msg=msg)
