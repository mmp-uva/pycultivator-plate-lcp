"""
This module provides retrieving and storing functionality for the 24-well plate light calibration plate
"""

from pycultivator.config import baseConfig as baseConfig
from pycultivator.config.baseConfig import (
    CalibrationConfig,
    PolynomialConfig,
    SettingsConfig
)
from pycultivator_plate_lcp import LCPlate, LCPConnection, Diode

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class LCPlateConfig(baseConfig.DeviceConfig):
    """Config class to create/save/update the device objects from/to the configuration source"""

    _configures = LCPlate
    _configures_types = {"plate"}
    required_source = "pycultivator_plate_lcp.config.baseConfig.Config"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(LCPlateConfig, cls).assert_source(source, expected)


class ConnectionConfig(baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a connection object from a configuration source"""

    _configures = LCPConnection
    required_source = "pycultivator_plate_lcp.config.baseConfig.Config"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(ConnectionConfig, cls).assert_source(source, expected)


class DiodeConfig(baseConfig.InstrumentConfig):
    """Config class to handle the diode configuration from the configuration source"""

    _configures = Diode
    _configures_types = {"diode"}
    required_source = "pycultivator_plate_lcp.config.baseConfig.Config"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = Config
        return super(DiodeConfig, cls).assert_source(source, expected)


class Config(baseConfig.Config):
    """ The config class provides the API to configure a light calibration plate object."""

    namespace = "config"
    # set available helpers
    _known_helpers = [
        # baseConfig.ObjectConfig,
        # baseConfig.PartConfig,
        LCPlateConfig,
        ConnectionConfig,
        # WellConfig,
        DiodeConfig,
        CalibrationConfig,
        PolynomialConfig,
        SettingsConfig
    ]

    default_settings = {
        # default settings for this clas
    }

    def __init__(self, settings=None, **kwargs):
        super(Config, self).__init__(settings, **kwargs)

    @classmethod
    def load(cls, location, settings=None, **kwargs):
        """ABSTRACT Loads the source into memory and creates the config object

        :param location: Location from which the configuration will be loaded
        :type location: str
        :param settings: PCSettings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_plate_lcp.config.baseConfig.Config
        """
        return super(Config, cls).load(location, settings=settings, **kwargs)


class ConfigException(baseConfig.ConfigException):
    """An Exception raised by the XMlConfig classes"""

    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)
