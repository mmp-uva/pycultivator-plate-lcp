"""
Module testing the pycultivator_plate_lcp configuration classes
"""

from pycultivator.config.tests import test_baseConfig
from pycultivator_plate_lcp.config import baseConfig

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SubjectTestConfig(test_baseConfig.TestConfig):
    """Class for testing the pycultivator Config object"""

    _abstract = True
    _subject_cls = baseConfig.Config


class TestLCPlateConfig(test_baseConfig.TestDeviceConfig):
    """Class for testing the pycultivator ComplexDeviceConfig object"""

    _abstract = True
    _subject_cls = baseConfig.LCPlateConfig


class TestConnectionConfig(test_baseConfig.TestConnectionConfig):
    """"""

    _abstract = True
    _subject_cls = baseConfig.ConnectionConfig


class TestDiodeConfig(test_baseConfig.TestInstrumentConfig):
    """Class for testing the XMLComplexDeviceConfig object"""

    _abstract = True
    _subject_cls = baseConfig.DiodeConfig

