"""Module for testing the pycultivator_plate_lcp XML Configuration"""

from pycultivator_plate_lcp.config import xmlConfig
from pycultivator.config.tests import test_xmlConfig
from pycultivator.core import pcXML
import test_baseConfig
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

SCHEMA_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "schema")
CONFIG_PATH = os.path.join(SCHEMA_DIR, "configuration.xml")
SCHEMA_PATH= os.path.join(SCHEMA_DIR, "pycultivator_plate_lcp.xsd")


class TestXMLObjectConfig(test_xmlConfig.TestXMLObjectConfig):

    _abstract = True
    config_location = CONFIG_PATH

    @classmethod
    def getConfguration(cls):
        result = xmlConfig.XMLConfig.load(CONFIG_PATH)
        if result is None:
            raise ValueError("Unable to load configuration")
        return result


class TestXMLLCPlateConfig(
    TestXMLObjectConfig,
    test_xmlConfig.TestXMLDeviceConfig,
    test_baseConfig.TestLCPlateConfig
):

    _abstract = False
    _subject_cls = xmlConfig.XMLLCPlateConfig

    def getSubject(self):
        """Return the subject being tested

        :rtype: pycultivator_plate_lcp.config.xmlConfig.XMLLCPlateConfig
        """
        return super(TestXMLLCPlateConfig, self).getSubject()

    def test_select(self):
        xml = xmlConfig.baseXMLConfig.pcXML.et.XML(
            """<lcp xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <instruments><led></led><led></led></instruments></lcp>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 1)
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </a>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 0)

    def test_is_valid_tag(self):
        e = pcXML.et.XML("<lcp></lcp>")
        self.assertTrue(self.getSubject().is_valid_tag(e))
        e = pcXML.et.XML("<bla></bla>")
        self.assertFalse(self.getSubject().is_valid_tag(e))
        root = self.getConfguration().getXML().getRoot()[0]
        self.assertTrue(self.getSubject().is_valid_tag(root))

    def test_create_definition(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                </a>""")
        panel = self.getDefaultObject()
        element = self.getSubject().create_definition(panel, root=xml)
        self.assertIsNotNone(element)
        self.assertEqual(self.subject.getXML().getCleanElementTag(element), self.subject.XML_ELEMENT_TAG)
        panels = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(panels), 1)
        connections = self.getSubject().find_elements("connection", root=xml, scope="//")
        self.assertEqual(len(connections), 0)
        instruments = self.getSubject().find_elements("instruments", root=xml, scope="//")
        self.assertEqual(len(instruments), 0)

    def test_save(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </a>"""
        )
        panel = self.getDefaultObject()
        self.subject.save(panel, root=xml)
        elements = self.subject.select(root=xml, scope="//")
        self.assertEqual(len(elements), 1)
        element = elements[0]
        self.assertEqual(self.subject.getXML().getCleanElementTag(element), self.subject.XML_ELEMENT_TAG)
        panels = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(panels), 1)
        connections = self.getSubject().find_elements("connection", root=xml, scope="//")
        self.assertEqual(len(connections), 0)
        instruments = self.getSubject().find_elements("instruments", root=xml, scope="//")
        self.assertEqual(len(instruments), 1)

    def test_write(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </a>"""
        )
        panel = self.getDefaultObject()
        element = self.subject.create_definition(panel, root=xml)
        panels = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(panels), 1)
        connections = self.getSubject().find_elements("connection", root=xml, scope="//")
        self.assertEqual(len(connections), 0)
        instruments = self.getSubject().find_elements("instruments", root=xml, scope="//")
        self.assertEqual(len(instruments), 0)
        self.assertIsNotNone(element)
        self.subject.write(panel, element)
        connections = self.getSubject().find_elements("connection", root=xml, scope="//")
        self.assertEqual(len(connections), 0)
        instruments = self.getSubject().find_elements("instruments", root=xml, scope="//")
        self.assertEqual(len(instruments), 1)

    def test_read(self):
        pass


class TestXMLDiodeConfig(
    TestXMLObjectConfig,
    test_xmlConfig.TestXMLInstrumentConfig,
    test_baseConfig.TestDiodeConfig
):
    """Class for testing the XMLSettingsConfig object"""

    _abstract = False
    _subject_cls = xmlConfig.XMLDiodeConfig

    def getSubject(self):
        """The subject being tested

        :rtype: pycultivator_plate_lcp.config.xmlConfig.XMLDiodeConfig
        """
        return super(TestXMLDiodeConfig, self).getSubject()

    def test_select(self):
        xml = xmlConfig.baseXMLConfig.pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <diode></diode><diode></diode></a>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 2)
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </a>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 0)

    def test_find(self):
        xml = xmlConfig.baseXMLConfig.pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <diode id="1"></diode><diode></diode></a>"""
        )
        diode = self.getDefaultObject()
        diode.name = 1
        element = self.getSubject().find_first(diode, root=xml)
        self.assertIsNotNone(element)
        # retrieve first led definition
        elements = self.getSubject().select()
        self.assertGreaterEqual(len(elements), 1)
        e = elements[0]
        diode = self.getSubject().read(e, self.getDefaultObject())
        self.assertIsNotNone(diode)
        self.assertEqual(diode.name, '0')
        # see if we retrieve the correct element
        element = self.subject.find_first(diode)
        self.assertIsNotNone(element)
        self.assertEqual(self.subject.read(element, self.getDefaultObject()).name, diode.name)

    def test_read(self):
        elements = self.getSubject().select(scope="//")
        self.assertGreaterEqual(len(elements), 1)
        self.assertLessEqual(len(elements), 24)
        element = elements[0]
        diode = self.subject.read(element, self.getDefaultObject())
        self.assertIsInstance(diode, self.getSubjectClass().configures())

    def test_create_definition(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                </a>""")
        diode = self.getDefaultObject()
        element = self.subject.create_definition(diode, root=xml)
        self.assertIsNotNone(element)
        self.assertEqual(self.subject.getXML().getCleanElementTag(element), self.subject.XML_ELEMENT_TAG)
        diodes = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(diodes), 1)

    def test_write(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                </a>""")
        diode = self.getDefaultObject()
        element = self.getSubject().create_definition(diode, xml)
        self.assertIsNotNone(element)
        # now write information
        self.subject.write(diode, element)
        # check info in element
        elements = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(diode.name))
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                </a>""")
        diode = self.getDefaultObject()
        diode.color = "yellow"
        self.assertEqual(diode.color, "yellow")
        element = self.getSubject().create_definition(diode, xml)
        self.assertIsNotNone(element)
        # now write information
        self.subject.write(diode, element)
        # check info in element
        elements = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(diode.name))

    def test_save(self):
        diode = self.getDefaultObject()
        xpath = "//pc:instruments/pc:diode"
        s = self.getSubject().getXML()
        # test save to existing element with matching id
        xml = pcXML.et.XML(
            """<plate_lcp xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <instruments><diode id="0"/></instruments></plate_lcp>""")
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        element = elements[0]
        self.subject.save(diode, element)
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(diode.name))
        # test save to existing element without id
        xml = pcXML.et.XML(
            """<plate_lcp xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <instruments><diode/></instruments></plate_lcp>""")
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        element = elements[0]
        self.subject.save(diode, element)
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(diode.name))
        # test save to existing element without matching id
        xml = pcXML.et.XML(
            """<plate_lcp xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <instruments><diode id="1"/></instruments></plate_lcp>""")
        self.subject.save(diode, xml)
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 2)
        o = self.subject.find_first(diode, root=xml)
        self.assertIsNotNone(o)
        self.assertEqual(self.subject.getXML().getElementId(o), str(diode.name))
        # test save to document without existing element
        xml = pcXML.et.XML(
            """<plate_lcp xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </plate_lcp>""")
        self.subject.save(diode, xml)
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(diode.name))
        # test save to document with existing element matching id
        xml = pcXML.et.XML(
            """<plate_lcp xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <instruments><diode id="0"/></instruments></plate_lcp>""")
        self.subject.save(diode, xml)
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(diode.name))
        # test with different root
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator"></a>""")
        self.subject.save(diode, xml)
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        elements = self.subject.select(root=xml)
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(diode.name))
