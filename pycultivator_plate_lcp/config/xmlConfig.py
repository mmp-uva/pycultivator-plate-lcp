"""
This module provides configuration of the 24-well plate light calibration plate from XML Files
"""

# load default pycultivator xml configuration classes
from pycultivator.config import xmlConfig as baseXMLConfig
from pycultivator.config.xmlConfig import (
    XMLCalibrationConfig,
    XMLPolynomialConfig,
    XMLSettingsConfig
)
# import base lcp configuration
import baseConfig as baseConfig
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class XMLLCPlateConfig(baseXMLConfig.XMLDeviceConfig, baseConfig.LCPlateConfig):
    """Config class for handling the Light Calibration Plate Object from XML"""

    XML_ELEMENT_TAG = "lcp"
    required_source = "pycultivator_plate_lcp.config.xmlConfig.XMLConfig"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLLCPlateConfig, cls).assert_source(source, expected)


class XMLConnectionConfig(baseXMLConfig.XMLConnectionConfig, baseConfig.ConnectionConfig):
    """Config class for handling configuration of connection objects from a XML Source"""

    required_source = "pycultivator_plate_lcp.config.xmlConfig.XMLConfig"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLConnectionConfig, cls).assert_source(source, expected)


class XMLDiodeConfig(baseXMLConfig.XMLInstrumentConfig, baseConfig.DiodeConfig):
    """Config class to handle the diode configuration from the configuration source"""

    XML_ELEMENT_TAG = "diode"
    required_source = "pycultivator_plate_lcp.config.xmlConfig.XMLConfig"

    @classmethod
    def assert_source(cls, source, expected=None):
        if expected is None:
            expected = XMLConfig
        return super(XMLDiodeConfig, cls).assert_source(source, expected)

    # Behaviour methods

    def read_properties(self, definition, obj):
        """Read the properties from the configuration source

        :param definition: lxml.etree._Element._Element
        :param obj: pycultivator_plate_lcp.diode.Diode
        :return: The updates diode
        :rtype: pycultivator_plate_lcp.diode.Diode
        """
        obj = super(XMLDiodeConfig, self).read_properties(definition, obj)
        """:type: pycultivator_plate_lcp.diode.Diode"""
        ain = self.xml.getElementAttribute(definition, "AIN", _type=int)
        mio0 = self.xml.getElementAttribute(definition, "MIO0", _type=int)
        mio1 = self.xml.getElementAttribute(definition, "MIO1", _type=int)
        mio2 = self.xml.getElementAttribute(definition, "MIO2", _type=int)
        address = (ain, mio0, mio1, mio2)
        if None not in address:
            obj.setAddress(address)
        return obj

    def write_properties(self, obj, definition):
        """Read the properties from the configuration source

        :param definition: lxml.etree._Element._Element
        :param obj: pycultivator_plate_lcp.diode.Diode
        :return: Whether it was successful
        :rtype: bool
        """
        result = super(XMLDiodeConfig, self).write_properties(obj, definition)
        if result:
            if obj._getAddress() is not None:
                ain, mio0, mio1, mio2 = obj.getAddress()
                self.xml.setElementAttribute(definition, "AIN", ain)
                self.xml.setElementAttribute(definition, "MIO0", mio0)
                self.xml.setElementAttribute(definition, "MIO1", mio1)
                self.xml.setElementAttribute(definition, "MIO2", mio2)
                result = True
        return result


class XMLConfig(baseXMLConfig.XMLConfig, baseConfig.Config):
    """Config class for handling the Light Calibration Plate XML File"""

    # set available helpers
    _known_helpers = [
        # baseXMLConfig.XMLObjectConfig,
        # baseXMLConfig.XMLPartConfig,
        XMLLCPlateConfig,
        XMLConnectionConfig,
        # XMLWellConfig,
        XMLDiodeConfig,
        XMLCalibrationConfig,
        XMLPolynomialConfig,
        XMLSettingsConfig,
    ]

    XML_SCHEMA_PATH = os.path.join("schema", "pycultivator_plate_lcp.xsd")
    XML_SCHEMA_COMPATIBILITY = {
        "1.1": 2,
        "1.0": 0,
        # version: level
        # level 2: full support
        # level 1: deprecated
        # level 0: not supported
    }

    @classmethod
    def load(cls, path, settings=None, **kwargs):
        """Loads the source into memory and creates the config object

        :param path: Location from which the configuration will be loaded
        :type path: str
        :param settings: PCSettings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_plate_lcp.config.baseConfig.Config
        """
        return super(XMLConfig, cls).load(path, settings=settings, **kwargs)


class XMLConfigException(baseConfig.ConfigException):
    """Exception raised by the Plate XML Config"""

    def __init__(self, msg):
        super(XMLConfigException, self).__init__(msg)
