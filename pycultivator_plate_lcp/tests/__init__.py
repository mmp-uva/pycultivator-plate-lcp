"""Core test classes

Handles basic configuration
"""

from __future__ import print_function
import os
import sys

import pkg_resources
from pycultivator.core.tests import UvaTestCase
from pycultivator_plate_lcp.config import xmlConfig

if sys.version_info[0] < 3:
    input = raw_input

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class LiveLCPlateTestCase(UvaTestCase):
    """TestCase class for live functionality tests, these tests require a working plate"""

    LC_PLATE_CONFIG_PATH = pkg_resources.resource_filename(
        __name__, os.path.join("..", "config", "schema", "configuration.xml")
    )

    _use_fake = os.getenv("USE_FAKE", "1") == "1"
    _lc_plate = None

    @staticmethod
    def warn():
        from pycultivator.contrib.mixins.script.console import ConsoleInputMixin
        cim = ConsoleInputMixin()
        result = cim.read_confirmation(
            "############ !! ATTENTION !! ############ \n" +
            "This script requires a Light Calibration Plate\n" +
            "Check that the plate is connected to the CORRECT 24-plate\n" +
            "############ !! ATTENTION !! ############\n",
            is_strict=True
        )
        if not result:
            print ("Please fix this before calling this script! Exiting...")
        return result

    @staticmethod
    def normalize(signals, blanks):
        results = []
        if not isinstance(signals, (tuple, list)):
            signals = [signals]
        if not isinstance(blanks, (tuple, list)):
            blanks = [blanks]
        for idx, signal in enumerate(signals):
            blank = 0.0 if idx > len(blanks) else blanks[idx]
            results.append(signal - blank)
        return results

    @classmethod
    def loadLCPlate(cls, path=None, settings=None, **kwargs):
        """ Loads the configuration object and device from the given XML Configuration file path into a new script

        :param path: Path to the XML Configuration file
        :type path: str
        :param settings: PCSettings to be passed to the configuration
        :type settings: None or dict
        :return:
        :rtype: pycultivator_plate.device.Plate.Plate
        """
        result = False
        if path is None:
            path = cls.LC_PLATE_CONFIG_PATH
        c = xmlConfig.XMLConfig.load(path, settings=settings, **kwargs)
        if c is not None:
            # create device configuration object
            xdc = c.getHelperFor("device")
            # create device object
            d = xdc.load()
            """:type: pycultivator_plate_lcp.device.Plate.Plate"""
            if d is not None:
                cls.setLCPlate(d)
                result = cls.hasLCPlate()
        return result

    @classmethod
    def getLCPlate(cls):
        """Return the plate object or nothing if no plate is set

        :rtype: pycultivator_plate_lcp.lcpPlate.Plate or None
        """
        return cls._lc_plate

    @classmethod
    def hasLCPlate(cls):
        """Returns whether a plate is set for this test

        :rtype: bool
        """
        return cls.getLCPlate() is not None

    @classmethod
    def setLCPlate(cls, plate):
        """Sets the plate object that is used in this test

        :type plate: pycultivator_plate_lcp.device.Plate.Plate
        """
        cls._lc_plate = plate
        return cls.getLCPlate()

    @classmethod
    def usesFake(cls):
        """Whether this test uses a fake plate connection, thus simulating a plate"""
        return cls._use_fake

    @classmethod
    def getWells(cls):
        """Return a list of wells in the plate, if a plate is set

        :rtype: None or list[pycultivator_plate_lcp.device.Plate.PlateLCPWell]
        """
        result = None
        if cls.hasLCPlate():
            result = cls.getLCPlate().getChannels().values()
        return result

    @classmethod
    def setUpClass(cls):
        """Prepares the class for running"""
        super(LiveLCPlateTestCase, cls).setUpClass()
        # see if a fake connection should be made
        cls.getLog().info(
            "Will {} simulate the connection to the Light Calibration Plate".format(
                "NOT" if cls.usesFake() else ""
            )
        )
        # load configuration classes
        cls.loadLCPlate(settings={"fake.connection": cls.usesFake()})
        if not cls.hasLCPlate():
            raise AssertionError("Unable to load configuration file")
        result = cls.getLCPlate().connect()
        if not result:
            raise AssertionError("Unable to connect to plate")

    @classmethod
    def tearDownClass(cls):
        if cls.getLCPlate().isConnected():
            result = cls.getLCPlate().disconnect()
            if not result:
                raise AssertionError("Unable to disconnect plate")

    @classmethod
    def report(cls, msg, end="\n"):
        sys.stderr.write("{}{}".format(msg, end))
        sys.stderr.flush()
