"""A global test script to test the functioning of the whole package"""

from pycultivator_plate_lcp.tests import LiveLCPlateTestCase

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class TestPyCultivatorPlateLCP(LiveLCPlateTestCase):
    """Tests the package"""

    PLATE_BOARD = 0
    ALL_WELLS = range(24)
    ALL_LCP_DIODES = range(13)

    def test_wells(self):
        for well in self.ALL_WELLS:
            with self.subTest("Well #{}".format(well)):
                self.measure_well(well)

    def measure_well(self, well):
        return self.getLCPlate().getConnection().readWell(well, board=self.PLATE_BOARD)

    def test_photo_diodes(self):
        # read
        for diode in self.ALL_LCP_DIODES:
            with self.subTest("Diode: #{}".format(diode)):
                self.measure_photo_diode(diode)

    def measure_photo_diode(self, diode):
        return self.getLCPlate().getConnection().readAIN(diode)

    def test_instruments(self):
        i_types = self.getLCPlate().getInstrumentTypes()
        self.assertEqual(i_types, {"light", "photodiode", "diode"})

    def test_measures(self):
        variables = self.getLCPlate().measures()
        self.assertEqual(variables, {"raw_intensity", "intensity", "voltage_intensity", "voltage_blank_intensity"})

    def test_measure(self):
        measurements = self.getLCPlate().measure("photodiode", "voltage_intensity")
        self.assertTrue(len(measurements) > 0)

    def test_control(self):
        success = self.getLCPlate().control("photodiode", "voltage_intensity", 100)
        self.assertFalse(success)

    def test_controls(self):
        variables = self.getLCPlate().controls()
        self.assertEqual(variables, set([]))
