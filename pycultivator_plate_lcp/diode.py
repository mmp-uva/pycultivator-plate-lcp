"""
The Diode Module provides the classes for modelling a Diode on a 24-Well Plate Incubator Calibration Plate.
"""

# from pycultivator_plate_lcp.data.DataModel import LightMeasurement
from pycultivator.core import Parser
from pycultivator.instrument import diode

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class Diode(diode.Diode):
    """A light calibration plate diode"""

    namespace = "24plate.lcp.diode"

    INSTRUMENT_TYPE_SYNONYMS = [
        "photodiode", "diode", "light"
    ]
    default_settings = {
        "resolution": 12,
        "gain": 2
    }

    _voltage_intensity = diode.InstrumentVariable(
        None, private=True
    )
    _voltage_blank_intensity = diode.InstrumentVariable(
        None, private=True
    )

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        """ Initialise a PhotoDiode

        :param identity: The name of this object
        :type identity: str
        :param parent: The parent of this object
        :type parent: pycultivator_plate_lcp.device.Plate.PlateLCPWell
        :param settings: PCSettings for this object
        :type settings: None or dict
        """
        super(Diode, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)
        # self._voltage_intensity.bindEvent("measure", lambda: self.getVoltageIntensity(True))
        # self._voltage_blank_intensity.bindEvent("measure", lambda: self.getVoltageBlankIntensity(True))

    def getParent(self):
        """ Return the parent of this instrument

        :return:
        :rtype: pycultivator_plate_lcp.device.Plate.PlateLCPWell
        """
        return super(Diode, self).getParent()

    def getDevice(self):
        """ Return the ComplexDevice that has this instrument

        :return:
        :rtype: pycultivator_plate_lcp.device.Plate.Plate
        """
        return self.getParent()

    def getIndex(self):
        """Return the index of this diode

        :rtype: int
        """
        return Parser.to_int(self.name, default=0)

    def getGainIndex(self):
        """Returns the gain to be used when reading from the diode

        :rtype: int
        """
        return self.getSetting("gain")

    def setGainIndex(self, gain):
        if gain is not None:
            self.setSetting("gain", gain)
        return self.getGainIndex()

    def getResolutionIndex(self):
        """Returns the resolution that should be used when reading from the diode

        :rtype: int
        """
        return self.getSetting("resolution")

    def setResolutionIndex(self, resolution):
        if resolution is not None:
            self.setSetting("resolution", resolution)
        return self.getResolutionIndex()

    @property
    def address(self):
        return self.getAddress()

    def getAddress(self):
        result = self._getAddress()
        if not isinstance(result, (tuple, list)) or len(result) != 4:
            result = self.guessAddress()
        return result

    def _getAddress(self):
        return self.getSetting("address")

    def setAddress(self, address):
        self.setSetting("address", address)
        return self._getAddress()

    def guessAddress(self):
        """Tries to guess the Address of this diode"""
        # channel indexes are numbered horizontally, A1, A2, ..., A6, B1, B2, ..., B6, C1, ..
        index = self.getIndex()
        # wells are numbered vertically A1, B1, C2, D1, A2, B2, C2, D2, ...
        # to correct from index we transform
        well = (index // 6) + (index % 6)*4
        return well

    def getVoltageIntensity(self, update=False):
        result = self.getRawIntensity(update=update)
        self._voltage_intensity.set(result)
        if update is True:
            self._voltage_intensity.commit()
        return self._voltage_intensity.get()

    @property
    def voltage_intensity(self):
        return self.getVoltageIntensity()

    def getVoltageBlankIntensity(self, update=False):
        if update:
            value = self.read_diode_voltage_blank()
            self._voltage_blank_intensity.set(value)
            self._voltage_blank_intensity.commit()
        return self._voltage_blank_intensity.get()

    # communication

    def _measure(self, source, **kwargs):
        super(Diode, self)._measure(source, **kwargs)
        if source is self._voltage_intensity:
            self.getVoltageIntensity(True)
        if source is self._voltage_blank_intensity:
            self.getVoltageBlankIntensity(True)

    def read_diode(self, gain=None, resolution=None):
        well = self.getAddress()
        board = self.getDevice().getBoardIndex()
        if gain is None:
            gain = self.getGainIndex()
        if resolution is None:
            resolution = self.getResolutionIndex()
        return self.getDevice().getConnection().readWell(
            well, board=board, resolutionIndex=resolution, gainIndex=gain
        )

    def read_diode_raw(self, replicates=None, resolution=None, gain=None):
        result = 0.0
        if replicates is None:
            replicates = self.getReplicates()
        for __ in range(replicates):
            v = self.read_diode(resolution=resolution, gain=gain)
            if v is not None:
                result += v
        if replicates > 0:
            result /= float(replicates)
        return result

    def read_diode_voltage(self, replicates=None, resolution=None, gain=None):
        return self.read_diode_raw(replicates=replicates, resolution=resolution, gain=gain)

    def read_diode_voltage_blank(self, resolution=None, gain=None):
        """Reads the offset voltage, created by the cable"""
        if gain is None:
            gain = self.getGainIndex()
        if resolution is None:
            resolution = self.getResolutionIndex()
        return self.getDevice().getConnection().readBlank(resolutionIndex=resolution, gainIndex=gain)

    def read_diode_intensity(self, replicates=None, resolution=None, gain=None):
        voltage = self.read_diode_raw(replicates=replicates, resolution=resolution, gain=gain)
        return self.calibrate(voltage)


class DiodeException(diode.DiodeInstrumentException):
    """An exception generated by the light calibration plate"""

    def __init__(self, msg):
        super(DiodeException, self).__init__(msg)
