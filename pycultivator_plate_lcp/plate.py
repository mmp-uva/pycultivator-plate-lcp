"""
The LightCalibrationPlate class provides an API to the Light Calibration Plate
"""

from pycultivator.core import Parser
from pycultivator.device import Device, DeviceException
from pycultivator_plate_lcp.connection import LCPConnection
from pycultivator_plate_lcp.diode import Diode

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class LCPlate(Device):
    """A 24-Well Plate Light Calibration Plate"""

    namespace = "device.lcp"
    default_settings = {
        "serialNr": None,
        "localID": None,
        "devNr": None
    }

    # connection settings
    required_connection = LCPConnection

    # instrument settings
    INSTRUMENTS = {
        Diode: 24
    }

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(LCPlate, self).__init__(identity, parent, settings=settings, **kwargs)

    def getConnection(self):
        """ Get the connection object to the Light Calibration Plate (if exists, otherwise None)

        :return: Connection Object to the Light Calibration Plate
        :rtype: None or pycultivator_plate_lcp.lcpConnection.LCPConnection
        """
        return super(LCPlate, self).getConnection()

    def getBoardIndex(self):
        return Parser.to_int(self.getName(), default=0)

    def getPhotoDiodes(self):
        return self.getInstrumentsOfClass(Diode)

    def getPhotoDiode(self, idx):
        result = None
        name = str(idx)
        if name in self.getPhotoDiodes().keys():
            result = self.getPhotoDiodes()[name]
        return result


class LCPlateException(DeviceException):
    """An exception raised by the 24-Well Plate Light Calibration Plate classes"""

    def __init__(self, msg):
        super(LCPlateException, self).__init__(msg)
