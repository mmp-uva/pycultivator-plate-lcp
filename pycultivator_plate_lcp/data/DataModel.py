# coding=utf-8
"""
Implements basic data objects
"""

from pycultivator.data import DataModel

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class LightRecord(DataModel.Record):
    """A light measurement from the light calibration plate"""

    _namespace = "lcp.data.record.light"
    _name = "LightRecord"

    _schema = DataModel.Record.mergeSchema(DataModel.Schema([
        DataModel.Variable("well", None, int, True),
        DataModel.Variable("voltage", None, float, True),
        DataModel.Variable("light.intensity", None, float),
    ]))

    def __init__(self, well, voltage, intensity=None, settings=None, properties=None, **kwargs):
        super(LightRecord, self).__init__(settings=settings, properties=properties, **kwargs)
        self["well"] = well
        self["voltage"] = voltage
        self["light.intensity"] = intensity

    def getWell(self):
        return self["well"]

    def getVoltage(self):
        return self["voltage"]

    def getIntensity(self):
        return self["light.intensity"]

    def setIntensity(self, intensity):
        self["light.intensity"] = intensity
        return self.getIntensity()


class LightMeasurement(LightRecord, DataModel.TimeRecord):

    _namespace = "lcp.data.measurement.light"
    _name = "PlateLightCalibrationMeasurement"

    _schema = DataModel.TimeRecord.mergeSchema(
        LightRecord.mergeSchema(
            {
                # add extra variables
            }
        )
    )

    def __init__(self, well, voltage, t=None, t_zero=None, settings=None, properties=None, **kwargs):
        # add to kwargs so TimeRecord can pick it up
        kwargs["t"] = t
        kwargs["t_zero"] = t_zero
        super(LightMeasurement, self).__init__(
            well=well, voltage=voltage, settings=settings, properties=properties,**kwargs
        )
