
from pycultivator.contrib.console.arguments import *
from pycultivator.contrib.console.exceptions import *

from pycultivator.contrib.console.commands.device import DeviceCommand
from pycultivator.contrib.console.exceptions.device import InvalidDeviceException, NotConnectedException
from pycultivator_plate_lcp.plate import LCPlate


class LCPlateCommand(DeviceCommand):
    """Generic plate command"""

    namespace = "lcplate"

    def execute(self):
        if not isinstance(self.device, LCPlate):
            raise InvalidDeviceException(self, self.device, LCPlate)
        return super(LCPlateCommand, self).execute()


class ReadLCPDiodeCommand(LCPlateCommand):
    """Read the value of one or more photodiode(s) on the light calibration plate"""

    name = "lcp read diode"
    command = "lcp read diode"
    arguments = [
        StringArgument("type"),  # read a well or diode
        IndexRangeArgument("index", takes_all=True),
        IntegerArgument("replicates", is_required=False, default=1),
    ]
    """:type: set[T <= pycultivator.console.commands.CommandArgument]"""
    description = "Reads the intensity value of the "

    def process(self, line):
        result = super(ReadLCPDiodeCommand, self).process(line)
        if not self.device.isConnected():
            name = self.get("device").value
            raise NotConnectedException(self, name)
        type_ = self.get("type").value
        if type_.lower() == "well":
            headers = ["well", "value"]
        elif type_.lower() == "diode":
            headers = ["diode", "value"]
        else:
            raise InvalidArgumentValueException(line, self.get("type"))
        values = self.collect()
        # print results
        self.console.print_table(values, headers)
        return result

    def collect(self):
        indexes = self.get("index")
        """:type: pycultivator.console.arguments.IndexRangeArgument"""
        type_ = self.get("type").value
        replicates = self.get("replicates").value
        values = []
        if type_.lower() == 'well':
            items = indexes.select(self.device.getWells().keys(), indexes.value)
            if len(items) == 0:
                self.info("No wells matched to given selection!")
            for well_idx in items:
                well = self.device.getWell(well_idx)
                value = well.getPhotoDiode().read_diode_intensity(replicates)
                values.append({'well': well_idx, 'value': value})
        if type_.lower() == 'diode':
            # get diode
            items = indexes.select(range(48), indexes.value)
            """:type: list[int]"""
            if len(items) == 0:
                self.info("No diodes matched to given selection!")
            for diode in items:
                value = self.device.connection.readPhotoDiode(diode)
                values.append({'diode': diode, 'value': value})
        return values


def load_commands():
    """Return a list of command classes"""
    return [

    ]
