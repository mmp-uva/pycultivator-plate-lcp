"""Initializes basic test classes"""

import time

from pycultivator_lab.data import SQLiteStore
from pycultivator_lab.script import BaseScript
from pycultivator_plate_lcp.config import xmlConfig


class BasePlateScript(BaseScript.BaseScript):

    COLORS = ['white', 'green', 'red', 'blue', 'all']

    def __init__(self, plate=None):
        super(BasePlateScript, self).__init__()
        self._plate = plate

    def getPlate(self):
        """Returns the device that mdoels the 24-well plate

        :rtype: pycultivator_plate_lcp.device.Plate.Plate
        """
        return self._plate

    def setPlate(self, plate):
        """Sets the plate used by this script

        :type plate: pycultivator_plate_lcp.device.Plate.Plate
        :rtype: pycultivator_plate_lcp.device.Plate.Plate
        """
        self._plate = plate
        return self.getPlate()

    def hasPlate(self):
        """Returns whether this script has a plate

        :rtype: bool
        """
        return self.getPlate() is not None

    # Behaviour

    def load(self, path, settings=None, **kwargs):
        """ Loads the configuration object and device from the given XML Configuration file path into a new script

        :param path: Path to the XML Configuration file
        :type path: str
        :param settings: PCSettings to be passed to the configuration
        :type settings: None or dict
        :return:
        :rtype: pycultivator_plate_lcp.device.Plate.Plate
        """
        result = False
        c = xmlConfig.XMLConfig.load(path, settings=settings, **kwargs)
        if c is not None:
            # create device configuration object
            xdc = c.getHelperFor("device")
            # create device object
            d = xdc.load()
            """:type: pycultivator_plate_lcp.device.LCPlate.LCPlate"""
            if d is not None:
                self.setPlate(d)
                result = self.hasPlate()
        return result

    def _prepare(self):
        result = False
        if self.hasPlate():
            result = self.getPlate().connect()
        self.log("Connect to the device: {}".format(result))
        return result

    def execute(self):
        raise NotImplementedError

    def _clean(self):
        result = False
        if self.hasPlate():
            result = self.getPlate().disconnect()
        self.log("Disconnect from the device: {}".format(result))
        return result

    @staticmethod
    def process_list(arg_list, arg_range):
        """Processes input that is given as either a single value or as a list"""
        result = arg_list
        if arg_range is not None and len(arg_range) == 1:
            result = range(arg_range[0])
        if arg_range is not None and len(arg_range) > 1:
            result = range(arg_range[0], arg_range[1])
        while result is None:
            v = raw_input("Give well number:")
            try:
                result = int(v)
            except ValueError:
                print "Invalid input, try again"
        if isinstance(result, (int, float)):
            result = [result]
        return result


class BaseLCPScript(BasePlateScript):
    """A script that sets the intensity of one or more colors of one or more LEDS"""

    MIN_WELL_INDEX = 0
    MAX_WELL_INDEX = 24

    def idxToAddress(self, idx, board=None):
        if board is None:
            board = self.getPlate().getBoardIndex()
        well = (idx // 6) + (idx % 6) * 4
        # add a fixed board index; we assume that the LCP is board 2, add 3 to AIN index.
        AIN = well // 8 + (board * 3)
        # get values between 0-7
        left = well % 8
        # mio 2 = is 1 if value is above or equal to 4
        MIO2 = left / 4
        left -= MIO2 * 4
        # mio 0 = is 1 if left is above or equal to 2
        MIO0 = left / 2
        left -= MIO0 * 2
        # mio 1 = is 1 if eleft is above or equal to 1
        MIO1 = left
        return AIN, MIO0, MIO1, MIO2

    def __init__(self, plate=None):
        super(BaseLCPScript, self).__init__(plate=plate)

    def execute(self):
        raise NotImplementedError


class SimpleLCPScript(BaseLCPScript):

    MIN_RESOLUTION_INDEX = 0
    MAX_RESOLUTION_INDEX = 12
    MIN_GAIN_INDEX = 0
    MAX_GAIN_INDEX = 15

    def __init__(self, plate=None):
        super(SimpleLCPScript, self).__init__(plate=plate)
        self._wells = []
        self._interval = 0.1
        self._pause = 1
        self._resolutionIndex = 12
        self._gainIndex = 1

    def getWells(self):
        return self._wells

    def setWells(self, wells):
        if wells is None:
            wells = []
        if isinstance(wells, str):
            wells = [wells]
        self._wells = wells
        return self.getWells()

    def countWells(self):
        return len(self.getWells())

    def hasWells(self):
        return self.countWells() > 0

    def getInterval(self):
        return self._interval

    def hasInterval(self):
        return self.getInterval() > 0

    def setInterval(self, interval):
        self.constrain(interval, 0, 5, name="interval", include=True)
        self._interval = interval
        return self.getInterval()

    def getPause(self):
        return self._pause

    def hasPause(self):
        return self.getPause() > 0

    def setPause(self, pause):
        self.constrain(pause, 0, 10, name="pause time", include=True)
        self._pause = pause
        return self.getPause()

    def getResolutionIndex(self):
        return self._resolutionIndex

    def setResolutionIndex(self, index):
        self.constrain(
            index, self.MIN_RESOLUTION_INDEX, self.MAX_RESOLUTION_INDEX, name="resolution index", include=True
        )
        self._resolutionIndex = index
        return self.getResolutionIndex()

    def getGainIndex(self):
        return self._gainIndex

    def setGainIndex(self, index):
        self.constrain(index, self.MIN_GAIN_INDEX, self.MAX_GAIN_INDEX, name="gain index", include=True)
        self._gainIndex = index
        return self.getGainIndex()

    def execute(self):
        raise NotImplementedError


class MonitorLCPScript(SimpleLCPScript):

    def __init__(self, plate=None):
        super(MonitorLCPScript, self).__init__(plate=plate)

    def execute(self):
        raise NotImplementedError

    def _monitor(self, well):
        """Monitor the voltage in a well index

        :param well: Index of the well to monitor
        :type well: int
        """
        self.log("Measure well {} every {} seconds. Press Ctrl-C to stop.".format(well, self.getInterval()))
        self.log("Address: AIN = {} | MIO0 = {} | MIO1 = {} | MIO2 = {}".format(*self.idxToAddress(well)))
        while True:
            try:
                value = self._measure(well)
                self.log(msg="\rWell {:3d} = {:.7f}".format(well, value), end="")
                if self.hasInterval():
                    time.sleep(self.getInterval())
            except KeyboardInterrupt:
                break
        self.log("\nDone")

    def _measure(self, idx):
        value = None
        if self.MIN_WELL_INDEX <= idx <= self.MAX_WELL_INDEX:
            AIN, MIO0, MIO1, MIO2 = self.idxToAddress(idx)
            self.getPlate().getConnection().setDOState(16, state=MIO0)
            self.getPlate().getConnection().setDOState(17, state=MIO1)
            self.getPlate().getConnection().setDOState(18, state=MIO2)
            value = self.getPlate().getConnection().measureAIN(AIN, self.getResolutionIndex(), self.getGainIndex())
        return value


class MeasureLCPScript(SimpleLCPScript):

    def __init__(self, plate=None):
        super(MeasureLCPScript, self).__init__(plate=plate)
        self._measurements = []

    def run(self):
        result = self.hasPlate() and self._prepare()
        try:
            result = result and self.execute()
            result = result and self.report()
        except Exception as e:
            self.getLog().critical("Error occurred while executing script:\n\t\t{}".format(e))
        finally:
            self._clean()
        return result

    def getMeasurements(self):
        """

        :return:
        :rtype: list[pycultivator_plate_lcp.data.DataModel.LightMeasurement]
        """
        return self._measurements

    def clearMeasurements(self):
        self._measurements = []

    def _measure(self, idx):
        """Measure a well index"""
        results = []
        if self.getPlate().hasChannel(idx):
            well = self.getPlate().getChannel(idx)
            results = well.measure()
        return results

    def report(self, results=None):
        result = False
        if results is None:
            results = self.getMeasurements()
        db = SQLiteStore.SQLiteStore.create("./measurements.db")
        if db.isConnected():
            print "Wrote {} measurements to {}".format(db.writeAll(results), db.getLocation())
            db.close()
            result = True
        return result
