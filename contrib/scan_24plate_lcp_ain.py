#!/bin/python
"""
Script for scanning AIN Channels in the LabJack.

The script will read a AIN channel at regular intervals, until ordered to measure the next well.
"""

from _script import MonitorLCPScript
from pycultivator.foundation import pcLogger
import argparse, os

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class MonitorAINScript(MonitorLCPScript):

    MIN_AIN_CHANNEL = 0
    MAX_AIN_CHANNEL = 127

    def __init__(self, plate=None):
        super(MonitorAINScript, self).__init__(plate=plate)

    def idxToAddress(self, idx, board=None):
        return idx, 0, 0, 0

    def execute(self):
        result = self.hasWells()
        if result:
            for well in self.getWells():
                try:
                    self._monitor(well)
                    if self.hasPause():
                        self.pause(self.getPause(), "Wait {} seconds, before moving to next well")
                except KeyboardInterrupt:
                    break
        return result

    def _measure(self, idx):
        value = None
        if self.MIN_AIN_CHANNEL <= idx <= self.MAX_AIN_CHANNEL:
            value = self.getPlate().getConnection().measureAIN(idx, self.getResolutionIndex(), self.getGainIndex())
        return value


if __name__ == "__main__":
    script = MonitorAINScript()
    # create command line parameter parser
    parser = argparse.ArgumentParser(description="Test the loading and usage of the LightCalibration Plate")
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Create a fake connection (useful when no Plate is available).')
    parser.add_argument('--config', help='Path to the configuration file')
    parser.add_argument('--well', dest="wells", action="append", type=int,
                        choices=range(MonitorAINScript.MAX_AIN_CHANNEL+1),
                        help='The well that should be measured')
    parser.add_argument('--range', dest="range", nargs=2, type=int,
                        choices=range(MonitorAINScript.MAX_AIN_CHANNEL+1),
                        help="A range of indexes to try.")
    parser.add_argument('--interval', type=float,
                        help="The interval at which to read the well (in seconds).")
    parser.add_argument('--pause', type=float,
                        help="Time to wait between measuring two well indexes (in seconds).")
    parser.add_argument('--resolution', type=int,
                        help="Sets the resolution index (see LabJack documentation)")
    parser.add_argument('--gain', type=int,
                        help="Sets the gain index (see LabJack documentation)")
    parser.add_argument('-v', '--verbose', dest="verbose", action="count",
                        help="Print log messages to console. Use multiple flags to increase log detail.")
    # load default parameters
    parser.set_defaults(use_fake=False, verbose=0, interval=0.5, pause=1, resolution=12, gain=1, config=None)
    # read the arguments
    print "Load and read options"
    args = vars(parser.parse_args())
    # set verbose level
    verbose = args.get("verbose")
    if verbose > 0:
        script.setVerbosity(verbose).addLogHandler()
    use_fake = args.get("use_fake")
    wells = args.get("wells")
    idx_range = args.get("range")
    if isinstance(idx_range, (list, tuple)) and len(idx_range) == 2:
        wells = range(*idx_range)
    while wells is None:
        v = raw_input("Give well number:")
        try:
            wells = int(v)
        except:
            print "Invalid input, try again"
    if isinstance(wells, (int, float)):
        wells = [wells]
    script.setWells(wells)
    script.setInterval(args.get("interval"))
    script.setPause(args.get("pause"))
    script.setResolutionIndex(args.get("resolution"))
    script.setGainIndex(args.get("gain"))
    config = args.get("config")
    if config is None:
        # path to the example configuration file
        config = os.path.join(os.path.dirname(__file__), "..", "examples", "configuration.xml")
    script.load(config, {"fake.connection": use_fake})
    if not script.hasPlate():
        print "Unable to load device"
        exit(-1)
    # execute the script
    script.run()
