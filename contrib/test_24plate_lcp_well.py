#!/bin/python
"""Small script to test the plate package"""

import argparse

from pycultivator.foundation import pcLogger
from pycultivator_lab.data import SQLiteStore
from pycultivator_plate_lcp.config import xmlConfig

if __name__ == "__main__":
    log = pcLogger.uvaLogger.createLogger()
    log.connectStreamHandler()

    # create command line parameter parser
    parser = argparse.ArgumentParser(description="Test the loading and usage of the LightCalibration Plate")
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Create a fake connection (useful when no Plate is available).')
    # load default parameters
    parser.set_defaults(use_fake=False)
    # read the arguments
    args = vars(parser.parse_args())
    # path to the example configuration file
    p = "/opt/mmp.uva/packages/pycultivator-lcp/examples/configuration.xml"
    print "Load and read configuration options"
    # create configuration object
    c = xmlConfig.XMLConfig.load(p)
    # create device configuration object
    xdc = xmlConfig.XMLLCPlateConfig(c)
    # create device object
    d = xdc.load()
    # create appropriate connection object for device
    print "Load connection"
    xconc = xmlConfig.XMLConnectionConfig(c)
    xconc.setSetting("fake.connection", args.get("use_fake", False))
    d.setConnection(xconc.load())
    # now have device use the connection object
    d.connect()
    print "Connect to device:", d.isConnected()
    if not d.isConnected():
        print "Unable to connect to device, check if it's connected"
        exit(0)
    # now measure the plate!
    measurements = d.measure()
    print measurements
    d.disconnect()
    print "Close connection to device:", not d.isConnected()
    db = SQLiteStore.SQLiteStore.create("./measurements.db")
    print "Storing measurements in a DataStore"
    if db.isConnected():
        print "Wrote {} measurements:".format(db.writeAll(measurements))
    db.close()
