#!/bin/python
"""
Script for scanning all the wells defined in the configuration source.

The script will measure one well at regular intervals, until ordered to measure the next well.
"""

from _script import MonitorLCPScript
from pycultivator.foundation import pcLogger
import argparse, os

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class MonitorScript(MonitorLCPScript):

    MIN_WELL_INDEX = 0
    MAX_WELL_INDEX = 24

    def idxToAddress(self, idx, board=0):
        """Returns the address from a well index"""
        address = (0, 0, 0, 0)
        if not self.getPlate().hasChannel(idx):
            raise ValueError("Invalid channel index - {}".format(idx))
        well = self.getPlate().getChannel(idx)
        keys = well.getPhotoDiodeKeys()
        if len(keys) > 0:
            diode = well.getPhotoDiode(keys[0])
            address = diode.guessAddress()
        return address

    def __init__(self, plate=None):
        super(MonitorScript, self).__init__(plate=plate)

    def execute(self):
        result = self.getPlate().countChannels() > 0
        if result:
            keys = self.getPlate().getChannels().keys()
            keys.sort()
            for key in keys:
                try:
                    self._monitor(key)
                    if self.hasPause():
                        self.pause(self.getPause(), "Wait {} seconds, before moving to next well")
                except KeyboardInterrupt:
                    break
        return result

    def _measure(self, idx):
        """Measure a well index"""
        value = None
        if self.getPlate().hasChannel(idx):
            well = self.getPlate().getChannel(idx)
            measurements = well.measure("photodiode", "intensity_voltage")
            if len(measurements) > 0:
                value = measurements[0]
        return value

if __name__ == "__main__":
    script = MonitorScript()
    # create command line parameter parser
    parser = argparse.ArgumentParser(description="Test the loading and usage of the LightCalibration Plate")
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Create a fake connection (useful when no Plate is available).')
    parser.add_argument('--config', help='Path to the configuration file')
    parser.add_argument('--interval', type=float,
                        help="The interval at which to read the well (in seconds).")
    parser.add_argument('--pause', type=float,
                        help="Time to wait between measuring two well indexes (in seconds).")
    parser.add_argument('--resolution', type=int,
                        help="Sets the resolution index (see LabJack documentation)")
    parser.add_argument('--gain', type=int,
                        help="Sets the gain index (see LabJack documentation)")
    parser.add_argument('-v', '--verbose', dest="verbose", action="count",
                        help="Print log messages to console. Use multiple flags to increase log detail.")
    # load default parameters
    parser.set_defaults(use_fake=False, verbose=0, interval=0.5, pause=1, resolution=12, gain=1, config=None)
    # read the arguments
    print "Load and read options"
    args = vars(parser.parse_args())
    verbose = args.get("verbose")
    if verbose > 0:
        level = 50 - (verbose * 10)
        # script.getRootLog().setLevel(level)
        sh = pcLogger.UVAStreamHandler()
        script.getRootLog().setLevel(level)
        script.getRootLog().addHandler(sh)
    use_fake = args.get("use_fake")
    script.setInterval(args.get("interval"))
    script.setPause(args.get("pause"))
    script.setResolutionIndex(args.get("resolution"))
    script.setGainIndex(args.get("gain"))
    config = args.get("config")
    if config is None:
        # path to the example configuration file
        config = os.path.join(os.path.dirname(__file__), "..", "examples", "configuration.xml")
    script.load(config, {"fake.connection": use_fake})
    if not script.hasPlate():
        print "Unable to load device"
        exit(-1)
    # execute the script
    script.run()
