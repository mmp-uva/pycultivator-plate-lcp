#!/bin/python
"""Small script to test the plate package"""

from _script import SimpleLEDScript
from pycultivator.foundation import pcLogger
import argparse, os

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class MeasureScript(SimpleLEDScript):

    def __init__(self, plate=None):
        super(MeasureScript, self).__init__(plate=plate)

    def execute(self):
        result = self.hasWells()
        for idx in self.getWells():
            if self.getPlate().hasChannel(idx):
                measurements = self.getPlate().getChannel(idx).measure()
                if len(measurements) > 0:
                    self.log("Well {}: AIN = {}".format(idx, measurements[0].getVoltage()))
                result = True
        return result

if __name__ == "__main__":
    script = MeasureScript()
    # create command line parameter parser
    parser = argparse.ArgumentParser(description="Test the loading and usage of the LightCalibration Plate")
    parser.add_argument('-n', action="store_true", dest='use_fake',
                        help='Create a fake connection (useful when no Plate is available).')
    parser.add_argument('--config', help='Path to the configuration file')
    parser.add_argument('--well', action="append", dest="wells", type=int,
                        help='The well that should be measured')
    parser.add_argument('-v', '--verbose', dest="verbose", action="count",
                        help="Print log messages to console")
    # load default parameters
    parser.set_defaults(use_fake=False, verbose=0, config=None)
    # read the arguments
    print "Load and read options"
    args = vars(parser.parse_args())
    verbose = args.get("verbose")
    if verbose > 0:
        level = 50 - (verbose * 10)
        # script.getRootLog().setLevel(level)
        sh = pcLogger.UVAStreamHandler()
        script.getRootLog().setLevel(level)
        script.getRootLog().addHandler(sh)
    use_fake = args.get("use_fake")
    script.setWells(args.get("wells"))
    colors = args.get('colors')
    if colors is None:
        colors = ["green"]
    script.setColors(colors)
    script.setIntensity(args.get("intensity"))
    script.setFlashTime(args.get("flash"))
    config = args.get("config")
    if config is None:
        # path to the example configuration file
        config = os.path.join(os.path.dirname(__file__), "..", "examples", "configuration.xml")
    script.load(config, {"fake.connection": use_fake})
    if not script.hasPlate():
        print "Unable to load device"
        exit(-1)
    # execute the script
    script.run()
